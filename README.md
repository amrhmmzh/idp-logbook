# My Engineering Logbook 

![UPM-Logo-Vector](/uploads/b0fe5cc0422a4d5261e7ae794574dc2f/UPM-Logo-Vector.png)
## Integrated Design Project Log 


|Nama: Amir Hamzah Bin Nor Azmi|Matric.No:198380|
|------|------|

|Logbook Entry: 01| Date: 5/11/2021|
|------|------|

|1. Agenda|
|------------|
|a) Get details on both the design requirements of thruster arms and gondola|
|b) Come out with initial ideas and designs for thruster arms.|

|2. Goals|
|------------|
|a) Brainstorm ideas|
|b) Measurements intake from the modelled exact HAU.|
|c) Selections of the measurements in the Top, side, bottom view.|
|d) Overall complete design measurements of physical HAU.|

|3. Decision to solve the problem|
|-------|
|a) Took a visit to Lab H 2.1 to look at the HAU model and the real time design.|

|4. Method to solve the problem|
|-----------|
|a) Take the real measurements of the HAU which is available.|
|b) Measurements: Tolerance was set according to the measurements to avoid greater significant on the error involved.|

|5. Justification when solving the problem|
|------------|
|a) To perform a prototype which looks good in terms of the measurements ratio set up in the CATIA programme.|
|b) To perform the good performance calculations or reviews on HAU model.|
|c) To have a good results on the Fluid Dynamics concept.|

|6. Impact of/after the decision chosen|
|-------------------|
|a) Have a good illustrations or imaginations on how to design the HAU in the CATIA (Airframe Design).|
|b) Think of the possibilities parameters involved (Lift, Drag, Thrust, Weight).|
|c) Possible CFD outcomes is accurate.|

|7. Our next step|
|----------|
|a) Designing process will begin in CATIA programme.|
|b) Have to review on the parameters involved from other subsystem group to have a better idea in performance calculations.|

